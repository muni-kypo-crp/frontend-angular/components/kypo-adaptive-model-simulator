### 18.0.0 Update to Angular 18.
* 082fab7 -- [CI/CD] Update packages.json version based on GitLab tag.
* 60fe132 -- Merge branch 'develop' into 'master'
* dee2dc7 -- Merge branch '11-update-to-angular-18' into 'develop'
* 7826559 -- Update angular.json to Angular 18
* e960607 -- Update to Angular 18
### 16.1.1 Update visualization dependencies versions.
* c6a6b32 -- [CI/CD] Update packages.json version based on GitLab tag.
* dc09caa -- Merge branch 'update-visualization-versions' into 'master'
* 1fcb1c2 -- Update training api lockfile
* 6414e30 -- update visualization versions
### 16.1.0 Update sentinel versions.
* 6d08308 -- [CI/CD] Update packages.json version based on GitLab tag.
* 8434194 -- Merge branch 'develop' into 'master'
* 70d6acf -- Merge branch 'update-sentinel-versions' into 'develop'
* c2fb5bf -- Update sentinel versions
### 16.0.1 Update sentinel components version.
* 9ef00d2 -- [CI/CD] Update packages.json version based on GitLab tag.
* c9824dd -- Merge branch 'update-sentinel-components-version' into 'master'
* 2a97e4e -- Update sentinel components version
### 16.0.0 Update to Angular 16.
* e9fac21 -- [CI/CD] Update packages.json version based on GitLab tag.
* e602d7b -- Merge branch '10-update-to-angular-16' into 'master'
* d0a5dd8 -- Update to Angular 16, update local issuer to keycloak
### 15.0.1 Resolve Angular Material migration.
* e515adc -- [CI/CD] Update packages.json version based on GitLab tag.
* 828682e -- Merge branch 'resolve-Angular-15-material' into 'master'
* 2807c52 -- Resolve new Material styling
### 15.0.0 Update to Angular 15.
* 5324787 -- [CI/CD] Update packages.json version based on GitLab tag.
* 3e2eaa8 -- Merge branch '9-update-to-angular-15' into 'master'
* b3a3b34 -- Update to Angular 15
### 14.1.1 Fix task stepper for post-training tool.
* 7c3ec53 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4d53993 -- Merge branch '8-fix-task-stepper-for-instance-simulator' into 'master'
* 21e898b -- Resolve "Fix task stepper for instance simulator"
### 14.1.0 Add support for an access phase for the pre-training tool. Fix computing of a trainees' performance.
* 8166c66 -- [CI/CD] Update packages.json version based on GitLab tag.
* d87ba21 -- Merge branch '7-fix-the-computation-of-the-trainees-path' into 'master'
* d27802d -- Resolve "Fix the computation of the trainees' path"
### 14.0.1 Remove unused css and move object creation to mapper.
* 52762e5 -- [CI/CD] Update packages.json version based on GitLab tag.
* 22d6dde -- Merge branch '4-refactor-model-simulator' into 'master'
* 77c31f1 -- Resolve "Refactor model simulator"
### 14.0.0 Angular 14 version of the tool.
